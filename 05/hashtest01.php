<!DOCTYPE html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
  </head>
  <body>
  <?php
        $me_data = [
        'fruit' => 'スイカ',
        'sport' => '野球',
        'town' => '横浜',
        'age' => 21,
        'food' => 'カレーライス'
        ];

        echo $me_data['town']; // 横浜 と表示される

        echo $me_data['age']; // 21 と表示される

        echo $me_data['school']; // エラーになる

        $me_data['age'] = 25; // 上書きされる


        var_dump($me_data); // 配列の中身がすべて表示される

  ?>
  <pre>
        <?php var_dump($me_data); ?>
  </pre>
